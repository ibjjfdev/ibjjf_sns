# frozen_string_literal: true

require 'action_controller/railtie'
module IbjjfSns
  class NotificationsController < ActionController::Base
    skip_before_action :verify_authenticity_token
    before_action :authenticate

    def sns
      render(
        json: '',
        status: IbjjfSns::Api::ResponseService
                .new(request.body).run
      )
    end

    private

    def authenticate
      authenticate_with_http_basic do |username, password|
        IbjjfSns::Authenticator.new(username, password).run
      end
    end
  end
end
