# frozen_string_literal: true

module IbjjfSns
  class Router
    def self.mount(router)
      router.send(
        :post,
        IbjjfSns.configuration.route => 'ibjjf_sns/notifications#sns'
      )
    end
  end
end
