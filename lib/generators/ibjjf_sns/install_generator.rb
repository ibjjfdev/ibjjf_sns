# frozen_string_literal: true

require 'rails/generators/base'
require 'rails/generators/migration'

module IbjjfSns
  class InstallGenerator < Rails::Generators::Base
    class_option :'log-requests', type: :boolean, default: false

    desc 'Install the ibjjf_sns easily for you'
    def generate_initializer
      initializer(
        'ibjjf_sns.rb',
        <<~TEXT
          # frozen_string_literal: true
          IbjjfSns.configure do |config|
            config.route         = '/notifications/sns'
            config.client_id     = 'set-a-user-here'
            config.client_secret = 'set-a-password-here'
            config.log_requests  = false
          end
        TEXT
      )
    end

    def include_route
      route 'IbjjfSns.routes(self)'
    end

    def generate_logs_migration
      return unless options['log-requests']

      # rubocop :disable Layout/LineLength
      generate(
        'migration',
        'create_sns_histories message_id:string:index response_status:integer:index request_type:string:index topic_arn:string:index message:json subject:string subscribe_url:string request_date:datetime'
      )
      # rubocop :enable Layout/LineLength
    end

    def generate_notification_service
      create_file(
        'app/services/notification_service.rb',
        <<~TEXT
          # frozen_string_literal: true

          class NotificationService < IbjjfSns::Api::NotificationService
            # build your notification logic here
            def run
              200
            end
          end
        TEXT
      )
    end
  end
end
