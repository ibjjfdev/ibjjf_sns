# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IbjjfSns::Api::ResponseService do
  let(:type) { 'Notification' }

  let(:subscribe) { "{\n  \"Type\" : \"SubscriptionConfirmation\",\n  \"MessageId\" : \"7d38173e-af44-4d31-8f25-4b8a8a8ab0b7\",\n  \"Token\" : \"2336412f37fb687f5d51e6e241da92fcfd035a3368ac6c78af0d6f6cb77d78ccf0dd7ad67abb60cb4d2362dff2f1f1fff9a14b7d94d89f0749d4b8e394511a32cf6a8ba44c2cf2f422f5b94b4bbd9c2f4bddc3660c1e8af687b350a6754322ce347b625cae9353c66ee82ff7192ad77d6799d69da45b5cbbf3c4ea421c751b8d\",\n  \"TopicArn\" : \"arn:aws:sns:us-east-2:735007327683:notification-test\",\n  \"Message\" : \"You have chosen to subscribe to the topic arn:aws:sns:us-east-2:735007327683:notification-test.\\nTo confirm the subscription, visit the SubscribeURL included in this message.\",\n  \"SubscribeURL\" : \"https://sns.us-east-2.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-east-2:735007327683:notification-test&Token=2336412f37fb687f5d51e6e241da92fcfd035a3368ac6c78af0d6f6cb77d78ccf0dd7ad67abb60cb4d2362dff2f1f1fff9a14b7d94d89f0749d4b8e394511a32cf6a8ba44c2cf2f422f5b94b4bbd9c2f4bddc3660c1e8af687b350a6754322ce347b625cae9353c66ee82ff7192ad77d6799d69da45b5cbbf3c4ea421c751b8d\",\n  \"Timestamp\" : \"2018-11-26T18:08:47.022Z\",\n  \"SignatureVersion\" : \"1\",\n  \"Signature\" : \"oadt69yg1J/mDZoX9oP93rEFhgGGNA/Sjhbesx+/8aRLI8i7Z9tlT4wJcbu8EctU1rU5MIUQSv3Pgqnx8gtEfjFiREINgpaoJLCL8o1j8J8Z4dCeohIKgxTH5xe5RDzarknYagLFBsah2TSJd5LpiaizuUR1M/9CCAwWbGp782d0SlUc+42b8S4Kic3QK47rU+m6bXnJTIXx2jEbieW9DqHGLlOeeP6VgnDKOp5tQBTBvU6QDs/aOGSyysQSe/unHArjWsEIJYm6+2skq0cPJVabHc62ajTLwikhpy7zXxhYdYKPriTo1MXhs0bosL8Q0SUIA3HkKAaoHp/uco9jdQ==\",\n  \"SigningCertURL\" : \"https://sns.us-east-2.amazonaws.com/SimpleNotificationService-ac565b8b1a6c5d002d285f9598aa1d9b.pem\"\n}" }
  let(:notification) { "{\"Type\":\"Notification\",\"MessageId\":\"884a7ae8-d6e4-5f44-8f60-2e63a510f657\",\"TopicArn\":\"arn:aws:sns:us-east-2:735007327683:notification-test\",\"Subject\":\"championship-result-updated\",\"Message\":\"{}\"}" }

  let(:params) { StringIO.new notification }

  subject { described_class.new(params).run }

  describe '#run' do
    context 'for notifications' do
      it 'call the notification_response method' do
        allow_any_instance_of(described_class)
          .to receive(:notification_response)
          .and_return(200)

        expect(subject).to eq(200)
      end
    end

    context 'for subscriptions' do
      let(:params) { StringIO.new subscribe }
      it 'call the subscribe_confirmation_response method' do
        allow_any_instance_of(described_class)
          .to receive(:subscribe_confirmation_response)
          .and_return(200)

        expect(subject).to eq(200)
      end
    end
  end
end
