# frozen_string_literal: true
IbjjfSns.configure do |config|
  config.route         = '/notifications/sns'
  config.client_id     = 'TEST'
  config.client_secret = 'TEST'
  config.log_requests  = false
end
