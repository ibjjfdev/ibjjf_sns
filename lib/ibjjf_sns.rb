# frozen_string_literal: true

require 'ibjjf_sns/engine'

require 'ibjjf_sns/configuration'
require 'ibjjf_sns/router'
require 'ibjjf_sns/authenticator'

module IbjjfSns
  module_function

  def configuration
    @configuration ||= Configuration.new
  end

  def routes(router)
    IbjjfSns::Router.mount(router)
  end

  def configure
    yield(configuration)
  end
end
