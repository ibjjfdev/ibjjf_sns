# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IbjjfSns::Router, type: :model do
  describe '#mount' do
    let(:router) { double('Router') }

    subject { described_class.mount(router) }

    before do
      IbjjfSns.configure do |config|
        config.route = '/notifications/sns'
      end
    end

    after { subject }

    it 'register a route path' do
      expect(router).to receive(:send).with(
        :post,
        IbjjfSns.configuration.route => 'ibjjf_sns/notifications#sns'
      )
    end
  end
end
