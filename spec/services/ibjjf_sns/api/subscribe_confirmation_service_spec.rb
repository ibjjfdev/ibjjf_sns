# frozen_string_literal: true

require 'rails_helper'
require 'net/http'

RSpec.describe IbjjfSns::Api::SubscribeConfirmationService do
  let(:subscribe_url) do
    'https://sns.us-east-2.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-east-2:735007327683:notification-test&Token=2336412f37fb687f5d51e6e241da92fcfd035a3368ac6c78af0d6f6cb77d78ccf0dd7ad67abb60cb4d2362dff2f1f1fff9a14b7d94d89f0749d4b8e394511a32cf6a8ba44c2cf2f422f5b94b4bbd9c2f4bddc3660c1e8af687b350a6754322ce347b625cae9353c66ee82ff7192ad77d6799d69da45b5cbbf3c4ea421c751b8d'
  end

  let(:response_double) do
    instance_double(
      'Response',
      code: '200',
      body:'<ConfirmSubscriptionResponse xmlns="http://sns.amazonaws.com/doc/2010-03-31/"><ConfirmSubscriptionResult><SubscriptionArn>arn:aws:sns:us-east-2:735007327683:notification-test:392fb0c4-a9e3-4feb-b854-2f764316fe36</SubscriptionArn></ConfirmSubscriptionResult><ResponseMetadata><RequestId>94a6f1c7-27b8-5091-a750-a5b11d29afe1</RequestId></ResponseMetadata></ConfirmSubscriptionResponse>'
    )
  end

  let(:topic_arn) { 'arn:aws:sns:us-east-2:735007327683:notification-test' }

  let(:service) { described_class.new(subscribe_url, topic_arn) }

  describe '#run' do
    before do
      allow(Net::HTTP)
        .to receive(:get_response)
        .with(URI(subscribe_url))
        .and_return(response_double)
    end

    context 'with valid params' do
      it 'return :ok' do
        expect(service.run).to eq(200)
      end
    end

    context 'with invalid params' do
      let(:topic_arn) { 'testing-asfsdf-asdfasf-testing' }

      it 'return :unprocessable_entity' do
        expect(service.run).to eq(422)
      end
    end
  end
end
