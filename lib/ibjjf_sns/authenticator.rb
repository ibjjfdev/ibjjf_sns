# frozen_string_literal: true

module IbjjfSns
  class Authenticator
    attr_reader :client_id, :client_secret

    def initialize(client_id, client_secret)
      @client_id = client_id
      @client_secret = client_secret
    end

    def run
      same_client_id? && same_client_secret?
    end

    private

    def same_client_id?
      IbjjfSns.configuration.client_id == client_id
    end

    def same_client_secret?
      IbjjfSns.configuration.client_secret == client_secret
    end
  end
end
