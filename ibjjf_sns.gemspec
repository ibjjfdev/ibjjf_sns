# frozen_string_literal: true

$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'ibjjf_sns/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'ibjjf_sns'
  spec.version     = IbjjfSns::VERSION
  spec.authors     = ['Rodrigo Souza']
  spec.email       = ['rodrigolimads@gmail.com']
  spec.summary     = 'Centralize all the logic of DB Sns notifications'

  spec.files = Dir['{app,lib}/**/*', 'Rakefile', 'README.md']

  spec.add_dependency 'rails', '~> 5.2.4', '>= 5.2.4.4'

  spec.add_development_dependency 'rspec-rails', '4.0.2'
  spec.add_development_dependency 'rubocop', '1.8.1'
  spec.add_development_dependency 'rubocop-performance', '1.9.2'
  spec.add_development_dependency 'simplecov', '0.16.1'
end
