# frozen_string_literal: true

module IbjjfSns
  class Engine < ::Rails::Engine
    isolate_namespace IbjjfSns
  end
end
