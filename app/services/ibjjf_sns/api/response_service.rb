# frozen_string_literal: true

module IbjjfSns
  module Api
    class ResponseService
      attr_reader :body

      def initialize(request_body)
        @request_body = request_body
        @body = BodyParserService.new(@request_body).run
      end

      def run
        case body.type
        when 'SubscriptionConfirmation' then subscribe_confirmation_response
        when 'Notification' then notification_response
        else
          # Notification type not implemented
          422
        end
      end

      private

      def subscribe_confirmation_response
        SubscribeConfirmationService
          .new(body.subscribe_url, body.topic_arn)
          .run
          .tap { |code| log_request(code) if IbjjfSns.configuration.log_requests? }
      end

      def notification_response
        log = log_request if IbjjfSns.configuration.log_requests?

        ::NotificationService
          .new(body.subject, body.message)
          .run
          .tap { |code| log&.update_attribute(:response_status, code) }
      end

      def log_request(response_status = nil)
        ::SnsHistory.create(
          message_id: body.message_id,
          request_type: body.type,
          topic_arn: body.topic_arn,
          message: body.message,
          subject: body.subject,
          subscribe_url: body.subscribe_url,
          request_date: body.timestamp,
          response_status: response_status
        )
      end
    end
  end
end
