# frozen_string_literal: true

module IbjjfSns
  class Configuration
    attr_accessor :client_id, :client_secret, :route, :log_requests

    def log_requests?
      log_requests
    end
  end
end
