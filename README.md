# IbjjfSns

An easy way to get, log and handle sns notifications.

* Serve an automatic and configurable route to receive notifications
* HTTP_Basic authenticated
* Automatic and configurable log registration on DB
* Easy notification handle per application

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ibjjf_sns'
```

And then execute:
```bash
bundle install
```

After that:

```bash
rake g ibjjf_sns:install
```

If you want to log all notifications received:

```bash
rake g ibjjf_sns:install --log-requests
```

## Usage

After run the install command, 
