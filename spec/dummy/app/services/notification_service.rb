# frozen_string_literal: true

class NotificationService < IbjjfSns::Api::NotificationService
  # build your notification logic here
  def run
    200
  end
end
