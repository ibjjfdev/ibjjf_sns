# frozen_string_literal: true

module IbjjfSns
  module Api
    class BodyParserService
      attr_accessor :params

      RequestBodyStruct = Struct.new(
        :type, :message_id, :topic_arn, :message,
        :subject, :subscribe_url, :timestamp
      )

      def initialize(request_body)
        @request_body = request_body
        @params = parsed_request_body
      end

      def run
        RequestBodyStruct.new(
          params['type'], params['message_id'], params['topic_arn'],
          parsed_message, params['subject'], params['subscribe_url'],
          params['timestamp']
        )
      end

      private

      def parsed_request_body
        return {} unless @request_body.is_a?(StringIO)

        JSON
          .parse(@request_body.read)
          .transform_keys(&:underscore)
      end

      def parsed_message
        body_type = params['type']
        message = params['message']

        return JSON.parse(message) if body_type.eql?('Notification')

        message
      end
    end
  end
end
