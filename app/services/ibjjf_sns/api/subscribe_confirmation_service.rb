# frozen_string_literal: true

module IbjjfSns
  module Api
    class SubscribeConfirmationService
      attr_reader :subscribe_url, :topic_arn

      def initialize(subscribe_url, topic_arn)
        @subscribe_url = subscribe_url
        @topic_arn = topic_arn
      end

      def run
        successful? && same_topic? ? 200 : 422
      end

      private

      def response
        @response ||= Net::HTTP.get_response(URI(subscribe_url))
      end

      def response_hash
        @response_hash ||= Hash.from_xml(response.body)
      end

      def successful?
        response.code == '200'
      end

      def same_topic?
        response_hash
          .dig('ConfirmSubscriptionResponse', 'ConfirmSubscriptionResult', 'SubscriptionArn')
          .include?(topic_arn)
      end
    end
  end
end
