# frozen_string_literal: true

module IbjjfSns
  module Api
    class NotificationService
      attr_reader :subject, :message, :parsed_message

      def initialize(subject, message)
        @subject = subject
        @message = message
        @parsed_message = message.deep_symbolize_keys
      end
    end
  end
end
