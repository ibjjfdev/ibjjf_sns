# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IbjjfSns::Authenticator, type: :model do
  let(:client) { 'TEST' }
  let(:secret) { 'TEST' }

  subject { described_class.new(client, secret) }

  describe '#run' do
    context 'with valid credentials' do
      it 'return true' do
        expect(subject.run).to be_truthy
      end
    end

    context 'without valid credentials' do
      let(:secret) { 'ops' }

      it 'return false' do
        expect(subject.run).to be_falsey
      end
    end
  end
end
